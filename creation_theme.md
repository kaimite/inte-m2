<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 27/08/2018
  Time : 10:57
-->

# Création d'un nouveau thème

Pour créer un thème sur Magento 2 il faut, au minimum créer les fichiers suivants : 

````
├── app
    └── design
        └── Vendor
            └── theme
                ├── media
                |   └── preview_default.jpg
                ├── registration.php
                └── theme.xml
````

Par exemple : 

````
├── app
    └── design
        └── RRS
            └── default
                ├── media
                |   └── preview_default.jpg
                ├── registration.php
                └── theme.xml
````

## theme.xml

````xml
<?xml version="1.0"?>

<theme xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:Config/etc/theme.xsd">
    <title>Vendor - Theme</title>
    <parent>Magento/blank</parent>
    <media>
        <preview_image>media/preview_default.jpg</preview_image>
    </media>
</theme>
````

`<parent>` est le thème dont il hérite.
`<preview_image` est le chemin vers l'image de preview du thème

## registration.php
````php
<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
	ComponentRegistrar::THEME,
	'frontend/Vendor/theme',
	__DIR__
);
````

## Application du thème

Dans l'admin il faut aller dans `Content -> Design -> Configuration` puis sélectionner le thème en cliquant sur `Edit`