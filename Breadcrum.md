<!-- 
 Copyright (c) 2014 - 2018.year TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 25/07/2018
  Time : 13:45
-->
 
 
# Breadcrums

## Block
block **breadcrumbs** dans le fichier **vendor\magento\module-theme\view\frontend\layout\default.xml**

## Template par défaut
/vendor/magento/module-theme/view/frontend/templates/html/breadcrumbs.phtml

## JSON-LD
Afin d'améliorer le SEO il est conseillé de fournir le code suivant aux moteurs de recherche.

Test du code généré sur : https://search.google.com/structured-data/testing-tool

**Exemple d'intégration**

````php
<?php
$breadCrumbJson = "";
$breadCrumbPos  = 1;

if ($crumbs && is_array($crumbs)) {
	$breadCrumbJson = [
		'@context'  => 'http://schema.org',
		'@type'     => 'BreadcrumbList',
		'itemListElement' => []
	];

	foreach ($crumbs as $crumbName => $crumbInfo) {
		$breadCrumbJson['itemListElement'][] = [
			'@type'     => 'ListItem',
			'position'  => $breadCrumbPos,
			'item'      => [
				'@id'       => $crumbInfo['link'],
				'name'      => $block->escapeHtml($crumbInfo['label'])
			]
		];
		$breadCrumbPos++;
	}
}
?>

<?php if (!empty($breadCrumbJson)) : ?>
    <script type="application/ld+json">
        <?= json_encode($breadCrumbJson) ?>
    </script>
<?php endif; ?>
````