<!-- 
 Copyright (c) 2014 - 2018.year TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 25/07/2018
  Time : 13:45
-->
 
# SideBar



## WishList
Pour supprimer le bloc **WishList**

Il est possible de le faire via l'admin : 

**Stores -> Configuration -> Settings** puis dans la section **Customers** cliquez sur **Wish List**

Sinon vous pouvez juste supprimer le bloc de la colonne en modifiant le fichier layout : 

`/app/design/frontend/<Vendor>/<theme>/Magento_Wishlist/layout/default.xml`

````xml
<referenceBlock name="wishlist_sidebar" remove="true" />
````

## Compare
Pour supprimer le block

`/app/design/frontend/<Vendor>/<theme>/Magento_Catalog/layout/default.xml`
````xml
<referenceBlock name="catalog.compare.sidebar" remove="true" />
````