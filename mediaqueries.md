<!-- 
 Copyright (c) 2014 - 2019 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 22/01/2019
  Time : 12:15
-->
# Les mediaqueries avec Magento

Pour regrouper ses mediaqueries Magento passe par les mixins 

````less
//
//  Desktop
//  _____________________________________________
.media-width(@extremum, @break) when (@extremum = 'min') and (@break = @screen__m) {

}



//
//  Mobile
//  _____________________________________________
.media-width(@extremum, @break) when (@extremum = 'max') and (@break = @screen__s) {

}
````

Mais cette façon de faire n'est pas "magique" remplacer les min / max ou la valeur du break ne garanti pas que le code soit pris en compte. 

Pour cela il faut vérifier si le code de Magento à prévu votre couple `@extremum`, `@break`. Il faut se rendre dans le fichier `lib/web/css/source/lib/_responsive.less` 

````less
// /**
//  * Copyright © Magento, Inc. All rights reserved.
//  * See COPYING.txt for license details.
//  */

//
//  Responsive
//  _____________________________________________

//
//  Media variables, that can be used for splitting styles into several files
//  ---------------------------------------------

@media-common: true; // Sets whether to output common styles (true|false)
@media-target: 'all'; // Sets target device for styles output (all|desktop|mobile)

//
//  Media width mixin used to group styles output based on media queries
//  ---------------------------------------------

.media-width(@extremum, @break) when (@extremum = 'max') and (@break = @screen__s) {
}

//
//  Style groups for 'mobile' devices
//  ---------------------------------------------

& when (@media-target = 'mobile'), (@media-target = 'all') {

    @media only screen and (max-width: @screen__m) {
        .media-width('max', (@screen__m + 1));
    }

    @media only screen and (max-width: (@screen__m - 1)) {
        .media-width('max', @screen__m);
    }

    @media only screen and (max-width: (@screen__s - 1)) {
        .media-width('max', @screen__s);
    }

    @media only screen and (max-width: (@screen__xs - 1)) {
        .media-width('max', @screen__xs);
    }

    @media only screen and (max-width: (@screen__xxs - 1)) {
        .media-width('max', @screen__xxs);
    }

    @media all and (min-width: @screen__s) {
        .media-width('min', @screen__s);
    }

}

//
//  Style groups for 'desktop' devices
//  ---------------------------------------------

& when (@media-target = 'desktop'), (@media-target = 'all') {

    @media all and (min-width: @screen__m),
    print {
        .media-width('min', @screen__m);
    }

    @media all and (min-width: (@screen__m + 1)),
    print {
        .media-width('min', (@screen__m + 1));
    }

    @media all and (min-width: @screen__l),
    print {
        .media-width('min', @screen__l);
    }

    @media all and (min-width: @screen__xl),
    print {
        .media-width('min', @screen__xl);
    }
}
````

Charge à vous de le completer au besoin, mais dans vos propres fichiers bien sur ! 

Le soucis c'est qu'il peut y avoir des chevauchements dans les mediaqueries. 

Par exemple si une propriété est définie dans le bloc 

````less
& when (@media-common = true) {
	
}
````
Elle ne sera pas écrasée par celle écrite dans 
`````less
//
//  Mobile
//  _____________________________________________
.media-width(@extremum, @break) when (@extremum = 'max') and (@break = @screen__s) {

}
`````

Il faut donc prévoir de placer certaines propriété dans un bloc ou un autre.

Autre soucis si déclarez des CSS dans le bloc 

````less
//
//  Desktop
//  _____________________________________________
.media-width(@extremum, @break) when (@extremum = 'min') and (@break = @screen__m) {

}
````

Elles seront définie pour une taille d'écran supérieur à 768px