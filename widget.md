<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 14/08/2018
  Time : 17:15
-->

# Création d'un widget 

## Création du contenu

Il faut commencer par créer le contenu du widget (block statique, CMS, autre)

`Content -> Element -> Block`

## Création du widget

Une fois le contenu créé on peut créer le widget

`Content -> Element -> Widgets`

Le widget se positionne dans un container du layout. Donc soit on utilise on container présent soit on créé un container spécifique pour son widget

````xml
<referenceContainer name="page.wrapper">
    <container name="before.header.container" label="Before Header Container" htmlTag="div" htmlClass="pre_header__wrapper" before="header.container"/>
</referenceContainer>
````

L'intitulé du `label` sera repris dans la liste dans l'admin.

