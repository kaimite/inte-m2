<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 26/07/2018
  Time : 08:57
-->
# Git
 
http://rogerdudler.github.io/git-guide/index.fr.html
 
## Demarrer un projet
````git
git clone URL dossier_cible
````

## Commencer un nouveau ticket
Repartir du master et se mettre à jour
````git
git checkout master
git pull origin master
````

Créer sa nouvelle branche
````git
git checkout -b nom_branche
````
Au boulot !!!
````git
git add fichier1 fichier2 etc.
git commit -m "#num_ticket : Description"
git pull
````
Vérifier les éventuels conflits
````git
git push origin nom_branche
````
