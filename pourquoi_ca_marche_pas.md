<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 16/08/2018
  Time : 12:12
-->


# Mais pourquoi ça marche pas ?  

## A vérifier en premier lieu

- Vider les caches du navigateur
- Vérifier les caches de Magento pour en supprimer au besoin : `php bin/magento cache:status`
- Vider les caches de Magento : `php bin/magento cache:flush`
- Est-ce que le fichier est bien en ligne sur la VM ? 

## Je modifie mon layout .xml mais rien ne change

- Vérifier le chemin du fichier .xml
- Vérifier le thème appliqué en admin
- Vérifier le type de thème. Magento a 3 types de thème : 
    - _Physical type_ : celui qu'on utilise tout le temps
    - _Virtual type_ : C'est un thème basé sur un thème physique qui peut être géré en admin en ajoutant des CSS... Peit détail important : **Les layouts ne peuvent pas être modifiés**
    - _Staging type_ : Pas beaucoup d'infos dessus pour le moment.
    
## J'essaie de modifier un template HTML pour Knock-out Js mais ça ne fonctionne pas
Les templates HTML sont des assets publiés dans le dossier `public`. 
Vous devez donc refaire un `grunt exec:nom_theme` pour publier votre template mis à jour
 
## Je viens de faire un nouveau fichier .less et quand je compile rien ne change
Après création d'un nouveau fichier .less il faut executer un grunt `exec:mon_theme` pour que la liste des fichiers publiées dans `/pub/static` soit mises à jour.
Une fois que c'est fait relancez un `grunt less:mon_theme` pour tout compiler.

## Erreur de compilation en production
Il se peut que la compilation des assets ne fonctionne pas en mode production. 

J'ai eu l'erreur suivante : 

````less
variable @media-common is undefined in file /opt/atlassian/pipelines/agent/build/var/view_preprocessed/pub/static/frontend/<Vendor>/<theme>/en_US/css/source/components/_buttons.less in _buttons.less on line 13, column 9
11| //  _____________________________________________
12| 
13| & when (@media-common = true) {
14| 
15| 	.button {
16| 
````

Cela peut paraitre bizarre car tout fonctionne en mode developpeur avec Grunt. 

La solution est d'ajouter dans le fichier `app/design/frontend/<Vendor>/<theme>/web/css/source/_theme.less` ou `app/design/frontend/<Vendor>/<theme>/web/css/source/_extend.less`
````less
@import (reference) "../source/lib/variables/_responsive.less";
@import (reference) "../source/lib/_responsive.less";
````

Va comprendre Charles ! 