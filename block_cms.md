<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 12/09/2018
  Time : 16:53
-->
# Les blocs CMS

## Ajouter un bloc CMS dans un layout

Pour ajouter un bloc CMS dans un layout il suffit de proceder de la manière suivante : 
````xml
<referenceContainer name="footer">
    <block class="Magento\Cms\Block\Block" name="block.statick.footer" after="-">
        <arguments>
            <argument name="block_id" xsi:type="string">footer</argument>
        </arguments>
    </block>
</referenceContainer>
```` 

Ainsi vous pouvez éditer cette partie directement dans l'admin. 

Si vous avez plusieurs storeViews il suffit de faire autant de bloc que de storeview. D'associer chaque bloc à sa storeview et de garder le même identifiant. 

Magento se chargera de sélectionner le bon bloc en fonction de la storeview courante.

## Les directives de blocs

Vous pouvez utiliser des directives pour rajouter des données dynamiques à votre bloc. Come par exemple la traduction d'un terme ou même un autre bloc statique.

Prenoms l'exemple d'un footer a x colonnes. 

Dans mon layout j'insère mon bloc cms comme vu plus haut. 

Et dans mon bloc je met le code suivant : 

````html
<ul class="flex_grid__container">
	<li class="flex_grid__item">{{block id="footer.column.1"}}</li>
	<li class="flex_grid__item">{{block id="footer.column.2"}}</li>
	<li class="flex_grid__item">{{block id="footer.column.3"}}</li>
	<li class="flex_grid__item">{{block id="footer.column.4"}}</li>
	<li class="flex_grid__item">{{block id="footer.column.5"}}</li>
</ul>
````

Il me suffit maintenant de créer les différents blocs correspondants aux différentes colonnes et je peux agir sur mon footer directement dans l'admin de Magento. 
Selon les CSS Mises en place je pourrais même ajouter / supprimer des colonnes. 

### Traduire un terme

````html
{{trans "string to translate"}}

{{trans "<em>Important message</em>"|raw}}

{{trans "string to %var" var="$variable"}}

{{trans "Hi %customer_name" customer_name=$order.getCustomerName()}}
````

### Ajouter un bloc
````html
{{block class="Magento\Newsletter\Block\Subscribe" name="static.newsletter" template="Magento_Newsletter::subscribe.phtml"}}
````
### Ajouter un bloc CMS

````html
{{block id="block_identifier"}}
````


Pour en savoir plus sur les directives vous pouvez consuler la page suivante : 
<https://www.atwix.com/magento-2/directives/>
