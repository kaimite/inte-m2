
<!--   
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.  
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>  
  
  Date : 25/07/2018  
  Time : 15:32  
-->  
   
# Javascript  
 
## Widget jQuery via requirejs-config  
Dans le fichier **requirejs-config.js** à la racine du dossier du module dans le theme  
`/app/design/frontend/<Vendor>/<theme>/Magento_Wishlist/requirejs-config.js`  
   
````javascript  
var config = {  
    map: {  
        "*": {  
            widgetName : "Module_Name/js/widget_file"  
        }  
    }  
};  
````  
   
### Fichier phtml  
````html  
<div data-mage-init='{"widgetName":{"option" : "value"}}'>  
    ...  
    ...  
    ...  
</div>  
````  
### Fichier JS  
  Template de base  
````javascript  
define(  
    ['jquery', 'mage/translate'],  
    function($) {  
        'use strict';  
  
        $.widget('tbd.widgetName', {  
            options: {  
                myVar : 'default value'  
            },  
  
            _create: function() {  
                // Votre code ici  
            }  
        });  
    }  
);  
````  
Les options transmises dans l'initialisation sont fusionnées avec les options du widget.   
L'élément HTML qui support l'initialsation du script (data-mage-init) est accessible via **this.element** dans la function _create()  
  
#### Attention  
Le noms du widget (_widgetName_) doit être **le même** dans les 3 fichiers pour que cela fonctionne.  
  
#### Traduction  
Avec la dépendance 'mage/translate' :   

````javascript  
$.mage.__('Enter Your message here');  
````  
 
Voir également : <https://devdocs.magento.com/guides/v2.2/frontend-dev-guide/translations/translate_theory.html>
  
# Initialisation du JS  
Pour initialiser et exécuter un code JS il est possible d'utiliser la directive "data-mage-init".   
Il y a plusieurs façons de faire :   
  
## Directement dans la balise HTML  
````html  
<div data-mage-init='{"widgetName":{"message" : "Bienvenue"}}'>  
    ...  
    ...  
    ...  
</div>  
````  
  
## Via une balise Script  
Pour gagner en lisibilité il peut être intéressant d'utiliser une balise script :   
````html  
<script type="text/x-magento-init">  
{  
    "#jquery_selecteur": {  
        "widgetName" : {  
            "message" : "Bienvenue"  
         }  
    }  
}  
</script>  
  
````  
Vous pouvez également utiliser le PHP pour générer le code JSON de configuration du widget.  
````php  
$dataMageInit = [  
   "tbdGDPR" => [  
      "gdprSaveUrl" => $this->getGdprSaveUrl(),  
      "nbDaysCookieAlert" => $this->getNbDaysCookieAlert(),  
      "cookieScript" => $this->getCookieScript(),  
      "useLoading" => $this->getDataHelper()->getUseLoading(),  
      "forceDisplayBadToken" => $this->getForceDisplay()  
   ]  
];  
  
````  
````html  
<div class="message" data-mage-init='<?= json_encode($dataMageInit) ?>'>  
  
````  
  
**Attention**  
  
Si vous utilisez la méthode inline via l'attribut _data-mage-init_ il faut utiliser des simples quotes et non des doubles :   
  
````  
data-mage-init='{...}' au lieu de data-mage-init="{...}"  
````

## Surcharge d'un fichier JS

### Dans un thème

De la même façon que pour les fichiers Layout XML ou les templates PHTML si un fichier JS existe dans votre dossier de thème il va remplacer le fichier d'origine de Magento. 

Par exemple : 

Le fichier Magento : `vendor/magento/module-checkout/view/frontend/web/js/view/summary.js`
````javascript
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/totals'
], function (Component, totals) {
    'use strict';

    return Component.extend({
        isLoading: totals.isLoading
    });
});
````

Le fichier du thème : `app/design/frontend/<Vendor>/<theme>/Magento_Checkout/web/js/view/summary.js`
````javascript

define([
    'uiComponent',
    'Magento_Checkout/js/model/totals',
    'Magento_Catalog/js/price-utils',
    'Magento_Checkout/js/model/quote'
], function (Component, totals, priceUtils, quote) {
    'use strict';

    return Component.extend({
        isLoading: totals.isLoading,

        /**
         * @return {Number}
         */
        getPureValue: function () {
            if (totals.totals()) {
                return parseFloat(totals.getSegment('grand_total').value);
            }

            return 0;
        },

        /**
         * @param {*} price
         * @return {*|String}
         */
        getFormattedPrice: function (price) {
            return priceUtils.formatPrice(price, quote.getPriceFormat());
        },

        /**
         * @return {*|String}
         */
        getValue: function () {
            return this.getFormattedPrice(this.getPureValue());
        }
    });
});

````

Ne pas oublier un petit `grunt exec` pour prendre en compte votre nouveau fichier !

### Dans un module

Vous pouvez également le faire dans un module de la façon suivante : 

Dans le fichier : `<Namespace>/<Module>/view/frontend/requirejs-config.js`

````javascript
var config = {
    map: {
        '*': {
            'Magento_Checkout/js/opc-checkout-method':'<Namespace>_<Module>/js/opc-checkout-method'
        }
    }
};
````

Et bien sur, créer le fichier `<Namespace>/<Module>/view/frontend/web/js/opc-checkout-method.js`