<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 31/08/2018
  Time : 14:53
-->

# Inscpecter les etats hover, active, ect.

Pour inspecter les différents états d'un element il suffit d'activer dans l'inspecteur l'option suivante : 

### Dans Chrome
![Inspecteur Chrome](img/inscpect-chrome.png "Inspecteur Chrome") 

### Dans FireFox
![Inspecteur Firefox](img/inscpect-firefox.png "Inspecteur Firefox") 