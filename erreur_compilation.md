<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 19/10/2018
  Time : 12:59
-->
## Erreurs de compilations des assets en mode production sur M2

Tout fonctionne bien en mode developer via `grunt less` mais une fois en production il y a des erreurs.

### variable @media-common is undefined in file ...
Il faut rajouter au début du fichier `<Vendor>/<theme>/web/css/source/_theme.less` ou `<Vendor>/<theme>/web/css/source/_extend.less`
les 2 lignes suivantes :

`````less
@import (reference) "../source/lib/variables/_responsive.less";
@import (reference) "../source/lib/_responsive.less";
`````

### Nom des fichiers less

J'ai eu des erreurs de compilations suite au nom du fichier. Le nom du fichier **doit** commencer par un underscore !

````text
monfichier.less -> KO
_monfichier.less -> OK
```` 