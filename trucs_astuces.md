<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 05/09/2018
  Time : 15:10
-->
# Petits trucs et astuces en vrac

## Faire disparaitre les messages d'infos au bout de xx secondes

### Solution full CSS

Editer le fichier template qui affiche les messages `app/design/frontend/<Vendor>/<theme>/Magento_Theme/templates/messages.phtml`

````php
<div data-bind="scope: 'messages'">
	<!-- ko if: cookieMessages && cookieMessages.length > 0 -->
	<div role="alert" data-bind="foreach: { data: cookieMessages, as: 'message' }" class="messages">
		<div data-bind="attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message hide_messages',
            'data-ui-id': 'message-' + message.type
        }">
			<div data-bind="html: message.text"></div>
		</div>
	</div>
	<!-- /ko -->
	<!-- ko if: messages().messages && messages().messages.length > 0 -->
	<div role="alert" data-bind="foreach: { data: messages().messages, as: 'message' }" class="messages">
		<div data-bind="attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message hide_messages',
            'data-ui-id': 'message-' + message.type
        }">
			<div data-bind="html: message.text"></div>
		</div>
	</div>
	<!-- /ko -->
</div>
<script type="text/x-magento-init">
    {
        "*": {
            "Magento_Ui/js/core/app": {
                "components": {
                        "messages": {
                            "component": "Magento_Theme/js/view/messages"
                        }
                    }
                }
            }
    }
</script>
````

J'ai ajouté la class `hide_messages` dans les blocs qui affichent le message (pour les cookieMessages et les messages). 

Cette class est en faite une transition qui masque le bloc et qui sera lancée au bout de 5 secondes. 

Le même principe qu'en JS mais en CSS !

````css
.hide_messages {
    animation-name : hideMessageKeyframes;
    animation-duration : 0.5s;
    animation-iteration-count : 1;
    animation-delay : 5s;
    animation-fill-mode : both;
}

@keyframes hideMessageKeyframes {
    from { top : 0; }
    to { top : -50px; }
}
````

### Solution Javascript

Editer le fichier template qui affiche les messages `app/design/frontend/<Vendor>/<theme>/Magento_Theme/templates/messages.phtml`

Dans le foreach de koJS on va rajouter une indication pour exécuter une fonction après le rendu : `afterRender:hideMageMessages`

`````php
<div data-bind="scope: 'messages'">
	<!-- ko if: cookieMessages && cookieMessages.length > 0 -->
	<div role="alert" data-bind="foreach: { data: cookieMessages, as: 'message', afterRender:hideMageMessages }" class="messages">
		<div data-bind="attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message',
            'data-ui-id': 'message-' + message.type
        }">
			<div data-bind="html: message.text"></div>
		</div>
	</div>
	<!-- /ko -->
	<!-- ko if: messages().messages && messages().messages.length > 0 -->
	<div role="alert" data-bind="foreach: { data: messages().messages, as: 'message', afterRender:hideMageMessages }" class="messages">
		<div data-bind="attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message',
            'data-ui-id': 'message-' + message.type
        }">
			<div data-bind="html: message.text"></div>
		</div>
	</div>
	<!-- /ko -->
</div>
<script type="text/x-magento-init">
    {
        "*": {
            "Magento_Ui/js/core/app": {
                "components": {
                        "messages": {
                            "component": "Magento_Theme/js/view/messages"
                        }
                    }
                }
            }
    }
</script>
`````

Dans le fichier `app/design/frontend/<Vendor>/<theme>/requirejs-config.js` rajoutez une dépendance

````javascript
var config = {
    deps: [
        'js/hide_message'
    ]
};
````

Et dans le fichier `app/design/frontend/<Vendor>/<theme>/web/js/hide_message.js`

````javascript
define(['jquery'],function(jQuery) {
    "use strict";

    window.hideMageMessages = function (element, message) {
        setTimeout(function(){
            jQuery(element[1]).hide();
        }, 5000);
    }
});
````