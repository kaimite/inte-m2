<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 14/08/2018
  Time : 10:33
-->
# Les petits trucs à vérifier sur les maquettes avant intégration

## Général

### Police

- La police utilisée est-elle dispo sur GoogleFont ?
- Si non est-elle dispo pour le web avec toutes le variantes demandées ?
- Tous les caractères de toutes les langues sont-ils disponibles ?
- Essayer de limiter le nombre de variantes 
    - Pour une police web à charger chaque variante est un nouveau fichier à charger : 
        - Regular
        - Bold
        - Regular Italic
        - Bold Italic
        - ...
        - ça peut donc vite grimper en terme de poids à charger pour l'internaute !

## Fiches produit 

- Présence du bloc de réassurrance (Infos pour rassurer le client : Paiement 100% sécurisé, Tél. du support, méthode de livraison, etc.)
- Description courte visible tout de suite (pour l'indexation), La description longue est affichée plus en bas dans la fiche. 
- Penser à comment va évoluer la ficher avec différents produits/langues : 
    - Plus de couleurs
    - Plus de tailles
    - Nom du produit sur 2 lignes (Par exemple avec des textes dans d'autres langues)
    - etc.
- Si on affiche la marque au dessus de la désignation du produit, est-ce cliquable et si oui vers quelle type de page ? 
- Affichage des différents prix (prix final / prix avant promotion)
- Superposition de pastilles sur le visuel (Nouveau, en promotion, etc.)


## Page catalogue
- Si le bouton "Ajouter au panier" n'est pas visible sur la maquette : 
    - Un ajout rapide au panier est-il prévu ? 
    - Si oui comment afficher le bouton "Ajout au panier"
- Etat de la fiche au survol
    - Affichage de nouvelles informations, changement de visuel, autre ?
- Affichage de la note et du nombre d'avis sur le produit

