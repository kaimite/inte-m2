<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 30/08/2018
  Time : 14:30
-->
# Importer un plugin JS

Dans le fichier `requirejs-config.js` déclarez le nouveau fichier: 

**L'extension `.js` n'est pas précisée.**

````javascript
var config = {
    paths : {
        'jqueryScrollbar'   : 'js/jquery.scrollbar-gh-pages/jquery.scrollbar.min'
    }
};
````  

Et pensez à préciser ses dépendences

````javascript
var config = {
    shim: {
        'jqueryScrollbar'   : {
            deps: ['jquery']
        }
    }
};
````  

Ce qui vous donne le code final : 

````javascript
var config = {
    paths : {
        'jqueryScrollbar'   : 'js/jquery.scrollbar-gh-pages/jquery.scrollbar.min'
    },
    shim: {
        'jqueryScrollbar'   : {
            deps: ['jquery']
        }
    }
};
````

Il peut maintenant être déclaré comme dépendance dans vos script JS : 

````javascript
define(['jquery', 'jqueryScrollbar'],function(jQuery) {
    jQuery('.scrollbar-outer').scrollbar({
        "showArrows": true,
        "scrollx": "advanced",
        "scrolly": "advanced"
    });
});
````