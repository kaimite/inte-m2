<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 26/07/2018
  Time : 10:31
-->
# LESS

A consulter également : <https://devdocs.magento.com/guides/v2.2/coding-standards/code-standard-less.html>
 
## Explication technique

De base dans Magento 2 il y a 3 fichiers CSS qui sont chargés : 

<https://github.com/magento/magento2/blob/2.0/app/design/frontend/Magento/blank/Magento_Theme/layout/default_head_blocks.xml>

Ces fichiers sont compilés depuis le theme parent. S'ils existent dans le dossier du thème alors ce seront eux qui seront pris en compte. 

Il n'y a pas d'héritage comme sur les layouts avec les fichiers LESS. S'il existe dans le dossier du thème, le fichier parent est ignoré !

Donc si vous avez dans `app/design/<Vendor>/<Theme>/web/css/` le fichier `styles-m.less` alors il remplacera intégralement le fichier du parent et vous aurez un theme **"from scratch"**

Si on regarde le fichier `styles-m.less` du thème Blank il est, au final, assez simple : 


````less
@import 'source/_reset.less';
@import '_styles.less';
@import (reference) 'source/_extends.less';

//
//  Magento Import instructions
//  ---------------------------------------------

//@magento_import 'source/_module.less'; // Theme modules
//@magento_import 'source/_widgets.less'; // Theme widgets

//
//  Media queries collector
//  ---------------------------------------------

@import 'source/lib/_responsive.less';

@media-target: 'mobile'; // Sets target device for this file

//
//  Global variables override
//  ---------------------------------------------

@import 'source/_theme.less';

//
//  Extend for minor customisation
//  ---------------------------------------------

//@magento_import 'source/_extend.less';

````

- Un petit reset
- Importation du fichier `_styles.less`
- Importation de `source/_extends.less`

Ensuite il y a une commande qui semble être en commentaire mais qui en réalité est une commande maison "Magento" : `//@magento_import 'source/_modules.less'`

Cette commande va parcourir le thème et chercher tous les fichiers `_module.less` dans les différents sous dossiers du thème pour les inclure : 

Elle donnera par exemple : 

````less
@import '../Magento_Catalog/css/source/_module.less';
@import '../Magento_Cms/css/source/_module.less';
@import '../Magento_Reports/css/source/_module.less';
@import '../Magento_Sales/css/source/_module.less';
````

La suite du fichier est simple. 

Pour styles-m la "target" est définie sur "mobile" et le fichier du theme est importé.

Et pour finir tous les fichiers `_extend.less` sont importés 


## Principes de compilation
 
Les fichiers .less sont compilés via Grunt dans 2 fichiers différents:

- styles-m.css
- styles-l.css
 
Magento 2 étant "Mobile First" le fichier styles-m.css contient la majeur partie du code et le fichier styles-l.css ne contient que les spécifications pour le desktop et tablettes. 
 
Le compilateur utilise les "& when()" et media_queries pour savoir dans quel fichier écrire la portion de code.
 
Par exemple si on prends le fichier suivant :
 
````less
.my_class {
    
}

& when (@media-common = true) {
    .class_commune {
        
    }
}

.media-width(@extremum, @break) when (@extremum = 'min') and (@break = @screen__l) {
	.class_desktop_via_mq {
	
	}
}

.media-width(@extremum, @break) when (@extremum = 'min') and (@break = @screen__m) {
	.class_tablette_via_mq {
	
	}
}

.media-width(@extremum, @break) when (@extremum = 'max') and (@break = @screen__m) {
	.class_mobile_via_mq {
	
	}
}
````

### Le fichier styles-m.css
Il contiendra les classes suivantes

- .my_class
- .class_commune
- .class_mobile_via_mq

### Le fichier styles-l.css
Il contiendra les classes suivantes

- .my_class
- .class_desktop_via_mq
- .class_mobile_via_mq

**Il est donc important de ne pas metre de code à la racine du fichier .less car il sera repris dans les 2 fichiers .css générés** 

Les déclarations apparaissent **dans le même ordre** que dans le fichier less.

## Cibler un device en particulier

Il est possible d'orienter une portion de code sans utiliser les médias queries.

````less
& when (@media-target = 'mobile') {
    .mobile {
        // Dans le fichier styles-m.css
    }
}

& when (@media-target = 'desktop') {
    .desktop {
        // Dans le fichier styles-l.css
    }
}
````

## MediaQueries et variables
Il n'est pas possible de définir des variables dans les médias queries. Pour changer la couleur de fond en fonction d'une media queries vous pouvez proceder de la facon suivante : 

````less
@block__background-mobile : white;
@block__background-desktop : green;
@block__background-tablet : orange;

& when (@media-common = true) {
    .block {
        background-color: @block__background-mobile;
    }
}

//
//  Desktop
//  _____________________________________________
.media-width(@extremum, @break) when (@extremum = 'min') and (@break = @screen__l) {
	.block {
        background-color: @block__background-desktop;
    }
}

//
//  Tablet
//  _____________________________________________
.media-width(@extremum, @break) when (@extremum = 'min') and (@break = @screen__m) {
	.block {
        background-color: @block__background-tablet;
    }
}
````

### Utilisation des variables CSS natives. 
CSS3 propose des variables que l'on peut utiliser dans les média queries : 

* <https://jsfiddle.net/pierre_louis__tbd/wcxtobhv/1/>

* <https://caniuse.com/#search=var()>

````css

:root {
  --main-bg-color : brown;
  --text-color : #FFF;
}

div {
  padding: 10px;
  color : var(--text-color);
  background-color: var(--main-bg-color);
  font-family : 'Arial';
  font-size: 15px;
}

@media screen and (max-width: 640px) {
  :root {
    --main-bg-color: Purple;
    --text-color : #000;
  }
}

````

````html
<div>Mon DIV</div>
````

Malheureusement ça casse le principe de LESS et le support par les principaux navigateurs n'est, à ce jour, pas complet. 

## Les mixins

Less permet de créer des "mixins", des functions ou portions de code réutilisables. 

Par exemple il est possible de faire ceci : 

````less
.default_shadow { // Sans parenthèse
    box-shadow: 0px 0px 20px 0px rgba(128,128,128,1);
}

.alert_box {
    .default_shadow; // Sans parenthèse
    border : 1px solid Red;
}

.success_box {
    .default_shadow; // Sans parenthèse
    border : 1px solid Green;
}
````

Mais il y a un petit inconvénient car la déclaration `.default_shadow` sera reprise dans le CSS généré : 

````css
.default_shadow {
  box-shadow: 0px 0px 20px 0px #808080;
}
.alert_box {
  box-shadow: 0px 0px 20px 0px #808080;
  border: 1px solid Red;
}
.success_box {
  box-shadow: 0px 0px 20px 0px #808080;
  border: 1px solid Green;
}
````

Il est préférable de procéder de la maniere suivante : 

````less
.default_shadow() { // Avec les parenthèses
    box-shadow: 0px 0px 20px 0px rgba(128,128,128,1);
}

.alert_box {
    .default_shadow(); // Avec les parenthèses
    border : 1px solid Red;
}

.success_box {
    .default_shadow(); // Avec les parenthèses
    border : 1px solid Green;
}
````

Et on obtient : 

````css
.alert_box {
  box-shadow: 0px 0px 20px 0px #808080;
  border: 1px solid Red;
}
.success_box {
  box-shadow: 0px 0px 20px 0px #808080;
  border: 1px solid Green;
}
````

## L'opérateur parent "&"

Il peut être très util si on souhaite adopter la nomenclature BEM pour ses CSS. 

Pour rappel le BEM est une convention de nommage de ses classes selon le principe BlocElementModifieur Par exemple : 
````css
.product_view {
    
}

.product_view__price {

}

.product_view__price--final {

}

.product_view__price--avant-promo {
    
}

````

Cette notation à l'avantage d'avoir, à chaque fois des déclaration avec une faible spécificité. 

Il sera plus simple de surcharger `.product_view__price--avant-promo` que `.product_view .price.avant-promo` par exemple. 

 Mais il est vrai que le HTML contient des classes plus longues. 
 
````html
<div class="product_view">
    <div class="product_view__price">
        <span class="product_view__price--avant-promo">250 €</span>
        <span class="product_view__price--final">250 €</span>
    </div>
</div>
````
Au lieu de 
````html
<div class="product_view">
    <div class="price">
        <span class="avant-promo">250 €</span>
        <span class="final">250 €</span>
    </div>
</div>
````

Donc si on veut faire du BEM en less on peut procéder de la facon suivante. 
Même si les déclarations sont imbriqués elles ne le seront pas dans le CSS compilé : 

````less
.product {
  display : block;
  padding : 10px;
  border  : 1px solid Gray;
  
  &__title {
    margin : 0 0 10px 0;
  }
  
  &__price {
    
    &--final {
      color: #000;
      font-size: 18px;
      font-weight: bold;
    }
    
    &--before {
      text-decoration: line-through;
      color: #333;
      font-size: 13px;
    }
  }
}
````
Ce qui donne le CSS suivant : 

````css
.product {
  display: block;
  padding: 10px;
  border: 1px solid Gray;
}
.product__title {
  margin: 0 0 10px 0;
}
.product__price--final {
  color: #000;
  font-size: 18px;
  font-weight: bold;
}
.product__price--before {
  text-decoration: line-through;
  color: #333;
  font-size: 13px;
}
````
Vous remarquerez que le `&` reprend tous les parents et pas uniquement le parent direct. 

Ainsi `&--final` donnera : `--final`  + son parent `__price` + son parent `.product` 

Cette technique peut être utilisée pour écrire des règles avec une partie du nom en commun : 

````less
.button {
  &-ok {
    background-image: url("ok.png");
  }
  &-cancel {
    background-image: url("cancel.png");
  }

  &-custom {
    background-image: url("custom.png");
  }
}
````
Donnera 

````css
.button-ok {
  background-image: url("ok.png");
}
.button-cancel {
  background-image: url("cancel.png");
}
.button-custom {
  background-image: url("custom.png");
}
````

## LESS et calc()

Lorsqu'on lance la compilation des fichiers LESS avec Grunt la commande CSS `calc()` est compilé par LESS et le résultat n'est pas celui attendu.

Par exemple : 

````less

.my_class {
    width : calc(100% - 10px);
}
````

Donnera : 

````css
.my_class {
    width : calc(90%);
}
````

Pour éviter cela il faut utiliser la syntaxe suivante : 

````less
.my_class {
    width : ~"calc(100% - 10px)";
}
````

En gros on préviens LESS de prendre notre chaine tel quelle sans la modifier. 
Petite précision : Les variables LESS sont tout de même interprétées mais écrites un peu différement.

````less
@gutter : 10px;
.my_class {
    width : ~"calc(100% - @{gutter})";
}
````