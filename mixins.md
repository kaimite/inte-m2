<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 23/08/2018
  Time : 11:04
-->
# Mixins less

Il y a un mixin dans magento 2 pour masquer visuellement des éléments. 

Si vous avez besoin d'afficher un element masqué utilisez le mixin  : 

````less
.lib-visually-hidden-reset()
````  

### Faire un cercle

````less
.circle(@radius: 5px, @border: 1px solid #000, @background: #fff) {
  display       : inline-block;
  width         : @radius;
  height        : @radius;
  border-radius : 50%;
  border        : @border;
  background    : @background;
}
````

### Alignement vertical via Flex

````less
.flexVerticalAlign() {
  display             : flex;
  justify-content     : center;
  align-items         : center;
  flex-direction      : column;
}
````
