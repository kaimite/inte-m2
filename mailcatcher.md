<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 05/09/2018
  Time : 10:46
-->
# Test des emails avec MailCatcher

Mailcatcher permet d'intercepter les emails envoyés et de les voir via une interface web. 

## Installation

Fait sur une VM en debian 8

### Installation de Ruby et de ses dépendances

````bash
sudo apt-get install ruby ruby-dev rubygems libsqlite3-dev
```` 

### Installation de MailCatcher
````bash
sudo gem install mailcatcher
````

A ce niveau j'ai eu l'erreur suivante : 

````text
ERROR:  Error installing mailcatcher:
        ERROR: Failed to build gem native extension.

    /usr/bin/ruby2.1 extconf.rb
checking for main() in -lcrypto... yes
checking for main() in -lssl... yes
checking for openssl/ssl.h... yes
checking for openssl/err.h... yes
checking for rb_trap_immediate in ruby.h,rubysig.h... no
checking for rb_thread_blocking_region()... yes
checking for ruby/thread.h... yes
checking for rb_thread_call_without_gvl() in ruby/thread.h... yes
checking for inotify_init() in sys/inotify.h... yes
checking for writev() in sys/uio.h... yes
checking for rb_thread_fd_select()... yes
checking for rb_fdset_t in ruby/intern.h... yes
checking for pipe2() in unistd.h... yes
checking for accept4() in sys/socket.h... yes
checking for SOCK_CLOEXEC in sys/socket.h... yes
checking for rb_wait_for_single_fd()... yes
checking for rb_enable_interrupt()... no
checking for rb_time_new()... yes
checking for sys/event.h... no
checking for epoll_create() in sys/epoll.h... yes
checking for clock_gettime()... yes
checking for CLOCK_MONOTONIC_RAW in time.h... yes
checking for CLOCK_MONOTONIC in time.h... yes
CXXFLAGS=-g -O2 -fstack-protector-strong -Wformat -Werror=format-security
creating Makefile

make "DESTDIR=" clean
sh: 1: make: not found

make "DESTDIR="
sh: 1: make: not found

make failed, exit code 127

````

J'ai trouvé la solution suivante : 

````bash
sudo apt-get install --reinstall build-essential
````

puis à nouveau 

````bash
sudo gem install mailcatcher
````

### Configuration de PHP

Editer le fichier php.ini et **son homologue pour php cli**
Par exemple : 

````text
sudo vim /etc/php5/apache2/php.ini
sudo vim /etc/php5/cli/php.ini
````

Recherchez et éditez les configurations suivantes : 
````php
smtp_port = 1025
sendmail_path = /usr/bin/env catchmail -f no-reply@mydomain.com
````

### Lancement de mailcatcher

Lancez la commande suivante : 

````bash
mailcatcher --http-ip=0.0.0.0
````

Et rendez-vous sur votre VM avec le port 1080 : 

<http://monprojet.com:1080/>