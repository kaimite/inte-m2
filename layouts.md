<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 07/08/2018
  Time : 11:09
-->

# Les layouts XML

<https://devdocs.magento.com/guides/v2.2/frontend-dev-guide/layouts/layout-overview.html>

Les layouts sont des fichiers XML qui permettent de composer la structure finale des pages du sites.
Ils permettent de créer, modifier, déplacer ou supprimer des informations sur la page. 

Pour faire simple ils sont composés de **containers** et de **blocks**

Le nom du layout est liée à l'url de la page appelée comme par exemple `catalog_category_view.xml`

Les layouts "default.xml" seront analysés à chaque page.

## Le container

Comme son nom l'indique il sert "juste" à contenir d'autres éléménts (blocks, widget ou autres containers)


````xml
<container name="container.name" as="container_name" label="Mon container"/>
````

Le label sera utilisé dans l'espace d'admin dans la partie "Widget" entre autres pour identifier le container et lui rajouter du contenu. 
Il est donc important.

## Le block
C'est un élément qui va afficher du contenu sur la page.
````xml
<block class="Magento\Framework\View\Element\Html\Link" name="block.name">
    ...
    ...
    ...
</block>

````

Le plus souvent le block est associé à une class PHP et à un template PHTML
`````xml
<block class="Magento\Framework\View\Element\Template" name="uikit.index.layout" template="TBD_Uikit::index/layout.phtml" />
`````

Dans le fichier template l'instance de la class est accessible via la variable **$block**
Dans le layout :

````xml
<block class="TBD\ChatBox\Block\ChatBox" name="product.info.chatbox" template="Magento_Catalog::product/chatbox_ko.phtml"/>
````
Et dans le template :
````php
$comments = $block->getComments(10, true);
````

## Ré-écrire une partie d'un layout

On va souvent avoir besoin de modifier les layouts standards pour les adapter à nos besoins. 
Pour cela il suffit de créer, dans notre thème, un fichier XML portant le même nom que le layout à étendre. 

Par exemple si je veux modifier la page qui affiche un produit. Le layout associé est : 
`vendor/magento/module-catalog/view/frontend/layout/catalog_category_view.xml`

Je vais donc créer dans mon thème le fichier suivant : 

`app/design/<Vendor>/<Theme>/Magento_Catalog/layout/catalog_category_view.xml`

`magento/module-catalog` => `Magento_Catalog` et on reprends l'arborescence en supprimant `view/frontend`

Ensuite, dans le fichier XML il suffit de mettre juste la modification. Inutile de reprendre tout le fichier. Magento se charge du reste. 


### Modifcation d'un bloc

`````xml
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" layout="2columns-left" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <referenceBlock name="category.products" template="Magento_Catalog::category/products_bis.phtml" />
    </body>
</page>
````` 

Je fait référence au bloc et j'indique un nouveau template. Toutes les directives définies dans le layout parent restent valides.

### Suppression du bloc

`````xml
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" layout="2columns-left" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <referenceBlock name="category.products" remove="true"/>
    </body>
</page>
````` 

### Déplacement d'un bloc
`````xml
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" layout="2columns-left" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <move element="category.products" destination="columns.top" before="-"/>
    </body>
</page>
````` 

## Ajouter du contenu à la page sans passer par un template
Il est possible d'utiliser le layout pour ajouter du contenu à la page sans passer par un template. 
Par exemple pour ajouter une liste de liens : 

````xml
<container name="sandbox.uikit.linkList" label="Link list" htmlTag="ul" htmlClass="uikit__linklist">
    <block class="Magento\Framework\View\Element\Html\Link" name="uikit.contact-link">
        <arguments>
            <argument name="label" xsi:type="string" translate="true">Contact us</argument>
            <argument name="path" xsi:type="string" translate="true">contact</argument>
        </arguments>
    </block>

    <block class="Magento\Framework\View\Element\Html\Link" name="uikit.facebook-link">
        <arguments>
            <argument name="label" xsi:type="string" translate="true">FaceBook</argument>
            <argument name="path" xsi:type="string" translate="true">facebook</argument>
        </arguments>
    </block>

    <block class="Magento\Framework\View\Element\Html\Link" name="uikit.twitter-link">
        <arguments>
            <argument name="label" xsi:type="string" translate="true">Twitter</argument>
            <argument name="path" xsi:type="string" translate="true">twitter</argument>
            <argument name="class" xsi:type="string" translate="true">link-twitter</argument>
        </arguments>
    </block>
</container>
````

On créé un container qui a un tag HTML et une class CSS.
Ensuite on créé des blocs basés sur la class PHP `Magento\Framework\View\Element\Html\Link` et on passe les arguments nécessaires à la création du lien.

Il est également possible d'ajouter directement du code HTML : 

````xml
<referenceContainer name="div.sidebar.additional">
    <block class="Magento\Framework\View\Element\Text">
        <action method="setText">
            <argument translate="true" name="text" xsi:type="string"><![CDATA[
                <h3>Heading</h3>
                <p style="text-align: justify; font-size: 10px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur error exercitationem, explicabo facere, id illum iste minus necessitatibus porro rem repellat repellendus similique sint temporibus tenetur, ullam ut vero voluptas.</p>
            ]]></argument>
        </action>
    </block>
</referenceContainer>
````

## Utiliser un layout comme base et le modifier

Sans forcément étendre un layout on peut avoir besoin de partir d'une base et de l'adapter / modifier selon nos besoin. 

Par exemple dans le module TBD_CssAudit j'ai 2 pages : 

- La page index
- la page de résultats

Les 2 pages ont une structure très similaire : 

- Le titre
- La liste des fichiers à analyser
- Le pré-requis pour l'execution de l'analyse

La page des résultats contient juste le résultat de l'analyse sous le titre. 

Les 2 fichiers de layout seront donc identique à un bloc prêt. 

### Fichier cssaudit_index_index.xml
````xml
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" layout="1column" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <referenceContainer name="content">
            <container name="tbd.audit.main">
                <block name="tbd.audit.title" template="TBD_CssAudit::index/title.phtml" />
                <block name="tbd.audit.file_list" class="TBD\CssAudit\Block\CssAudit" template="TBD_CssAudit::index/file_list.phtml" />
                <block name="tbd.audit.requirements" template="TBD_CssAudit::index/requirement.phtml" />
            </container>
        </referenceContainer>
    </body>
</page>
````

J'ai un container `tbd.audit.main` et les 3 blocs.
Pour ma page des résultats il me faut la même chose mais juste ajouter un bloc dans `tbd.audit.main` après `tbd.audit.title`

### Fichier audit_index_audit.xml
````xml
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" layout="1column" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <update handle="cssaudit_index_index"/>
    <body>
        <referenceContainer name="tbd.audit.main">
            <block class="TBD\CssAudit\Block\CssAudit" name="tbd.audit.audit" template="TBD_CssAudit::index/audit.phtml" after="tbd.audit.title" />
        </referenceContainer>
    </body>
</page>
````

Juste avant le `<body>` j'ai rajouté une balise `<update>` en lui précisant le nom du layout que je souhaite modifier. 
Ensuite il me reste juste a rajouter le bloc `tbd.audit.audit` apres `tbd.audit.title`

Le nom du layout est le nom du fichier XML sans extension.  