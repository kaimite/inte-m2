<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 12/10/2018
  Time : 14:22
-->
# Réalisation d'une font d'icones

## Vérification du dessin

Tout d'abord il faut exporter les icones au format SVG. Il ne faut pas de dessin avec des contours, uniquement des formes. 

Dans illustrator Vérifier en basculant le mode d'affichage `Ctrl + Y`

### Icone correcte
![Icone OK](img/-font-ico-1.png "Icone OK")

### Icone incorrecte
![Icone KO](img/-font-ico-2.png "Icone KO")

Sur cette version il y a un seul tracé avec une épaisseur. 
Pour corriger ça appliquez lui une epaisseur voulue puis faites `Objet -> Tracé -> Vectoriser le contour`  

## Vérification des masques d'écrétages
Selon l'origine du svg il se peut qu'il y ai des masques d'écrétage. 

Visuellement l'icone semble correcte mais lors de son import sur le site iconmoon il y a juste un gros carré : 

![Icone import](img/-font-ico-3.png "Icone import")

A gauche le svg exporté directement de Adobe XD, à droite le même retravaillé sur Illustrator.

Donc sur Illustrator vérifier les calques : 

![Masque ecretage](img/-font-ico-4.png "Masque ecretage")

Vous pouvez voir qu'il y a un calque "Ecrêter le groupe" et dans les autres calques un avec la miniature de votre icone en noir. 
C'est ce dernier qu'on va garder. 

Il suffit de le sélectionner et de le glisser juste en dessous du calque 1

![Masque ecretage](img/-font-ico-5.png "Masque ecretage")

Et de supprimmer les autres calques

![Masque ecretage](img/-font-ico-6.png "Masque ecretage")

Sur votre document vous ne voyez surement rien mais ce n'est pas grave. 

Sélectionnez tout `Ctrl + A / Sélection -> Tout` puis appliquez une couleur de fond et **aucun contour**.

![Remplissage forme](img/-font-ico-7.png "Remplissage forme")

## Harmonisation des icones

Les icones seront transormée en police de caractère. Il faut donc passer par une phase d'harmonisation pour pouvoir facilement les utiliser ensuite. 

On va donc les faire tenir dans un carré de 32px par 32px. 

La taille est arbitraire. De toutes façons la typo sera vectorielle... 

Donc maintenant que mon icone est propre on va tout sélectionner et le copier. `Ctrl + A / Ctrl + C`

Créer un nouveau document de 32px par 32px

![Nouveau doc](img/-font-ico-8.png "Nouveau doc")

Et on y colle notre icone.

Affichez la fenêtre des transformations `Fenêtre -> Transformations`

![Transformations](img/-font-ico-9.png "Transformations")

Modifiez la largeur ou la hauteur pour faire tenir l'icone dans une boite de 30px.
Placez le centre en 

- X : 16px
- Y : 16px

Voilà votre icone est centrée.

Il reste à enregistrer votre document en SVG.

Toutes vos icones auront la même taille et seront donc beaucoup plus simples à utiliser. 

##Création de la font

Rendez-vous sur : <https://icomoon.io/app/> 

Puis cliquez sur "Import Icons" et sélectionnez vos différents fichiers : 

![Transformations](img/-font-ico-10.png "Transformations")

![Transformations](img/-font-ico-11.png "Transformations")

Et vous avez normalement vos icones qui apparaissent

![Transformations](img/-font-ico-12.png "Transformations")

Dans la barre d'outils en haut cliquez sur le bouton de sélection et sélectionnez les icones à inclure dans la font. 
Vous pouvez cliquer les icones une à une ou faire un cliquer-glisser pour en sélectionner plusieurs d'un coup. 

![Transformations](img/-font-ico-13.png "Transformations")

![Transformations](img/-font-ico-14.png "Transformations")

En enfin cliquez sur `Generate Font` en bas de l'écran

![Transformations](img/-font-ico-15.png "Transformations")

Le détails de la font s'affiche et vous avez un bouton, en bas de la page pour télécharger la police avec toutes les infos dans le ZIP.

![Transformations](img/-font-ico-16.png "Transformations")

Vous pouvez également cliquez sur le bouton "Preferences" en haut de l'écran : 

![Transformations](img/-font-ico-17.png "Transformations")

## Attention
A chaque ajout d'icone il faudra re-générer la font complete, donc essayez de la faire une fois toutes les icones réunies ! 

N'oubliez pas de mettre à disposition le fichier généré ainsi que toutes les icones au format SVG si un autre intégrateur a besoin de re-générer la font pour ajouter/modifier des icones. 

### Autres ressources

<https://www.sodifrance.fr/blog/preparer-des-sources-svg-a-leur-integration-dans-une-police-dicones/>