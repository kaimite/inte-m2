<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 13/08/2018
  Time : 11:21
-->


# Création d'un composant JS avec KnockOut JS

En plus de jQuery, Magento utilise Knockout JS qui a l'avantage d'être compatible IE6... 

http://knockoutjs.com/index.html

Voici un exemple d'implémentation. 

L'exemple suivant affiche une liste de produit dans un tableau HTML. 

Nous avons donc un bloc PHP pour la récupération des produits et l'initialisation du composant ainsi qu'un template HTML qui affichera la liste. 

Pour faire simple nous allons remplacer l'affichage standard de le page product_category_view

Nous allons donc créer un nouveau fichier dans : 

`app/design/frontend/<Vendor>/<Theme>/Magento_Catalog/templates/product/list.phtml`

````php
//--> On créé un tableau pour KO.js avec la liste des produits

$_productCollection = $block->getLoadedProductCollection();
$koData             = [];

foreach ($_productCollection as $_product) {
	$koData[] = array(
		'title' => $block->stripTags($_product->getName(), null, true),
		'price' => $block->getProductPrice($_product)
	);
}

//--> Paramêtres d'initialisation du JS
$dataMageInit = [
	'#productKoGridDiv' => [
		'Magento_Ui/js/core/app' => [
			'components' => [
				'productKoGrid' => [
					'component' => 'Magento_Catalog/js/product_grid_ko',
					'template'  => 'Magento_Catalog/product_ko_grid',
					'products'  => $koData
				]
			]
		],
	],
];
````
````html
<div id="productKoGridDiv" data-bind="scope:'productKoGrid'">
	<h1>Les produits</h1>
	<!-- ko template: getTemplate() --><!-- /ko -->
</div>


<script type="text/x-magento-init">
	<?= json_encode($dataMageInit); ?>
</script>
````

J'ai donc un composant nommé `productKoGrid`. Ce sera le nom du scope : `data-bind=" scope:'productKoGrid' "`.
Ensuite je demande a afficher le résultat du template : 

`<!-- ko template: getTemplate() --><!-- /ko -->`

Mon fichier template est un fichier .html qui se situe dans : 

`app/design/frontend/<Vendor>/<Theme>/Magento_Catalog/web/template/product_ko_grid.html`

**Attention** : Nom du dossier est `template` sans "s".

### Le code du template
````html
<p>La liste des produits</p>
<table>
	<thead>
		<tr>
			<th>Produit</th>
			<th>Prix</th>
		</tr>
	</thead>
	<tbody data-bind="foreach: products">
		<tr>
			<td data-bind="text: title"></td>
			<td data-bind="html: price"></td>
		</tr>
	</tbody>
</table>
````

### Le code du fichier JS
````javascript
define(['uiComponent', 'ko'], function(Component, ko) {
    'use strict';

    var _self;
    return Component.extend({
        initialize: function () {
            this._super();
            _self = this;
            console.log("Products", _self.products);
            ...
            ...
            ...
        }
    });
});
````

Les produits qui ont été transmis en parametre dans l'initialisation du JS sont accessibles via `this.products`
