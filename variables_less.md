<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 01/10/2018
  Time : 15:30
-->
 
# Ou trouver les variables LESS pour Magento 2

## Boutons
`/vendor/magento/magento2-base/lib/web/css/source/lib/variables/_buttons.less`

````less
//  Font style
@button__font-family: @font-family__base;
@button__font-size: @font-size__base;
@button__font-weight: @font-weight__bold;
@button__line-height: @font-size__base + 2;
@button__margin: 0;
@button__padding: 7px 15px; // is set up to false when buttons don't depend on side paddings or have fixed width
@button__width: false; // is set up to false when buttons depend on side paddings or to fixed value when needed

//  Display settings
@button__display: inline-block;
@button__cursor: pointer;
@button__border-radius: 3px;

@button__disabled__opacity: .5;

//  Default = secondary button
@button__color: @primary__color;
@button__background: @color-gray95;
@button__border: 1px solid @color-gray-darken2;
@button__gradient-color-start: false;
@button__gradient-color-end: false;

@button__hover__color: @color-gray-darken3;
@button__hover__background: @color-gray-darken1;
@button__hover__border: @button__border;
@button__hover__gradient-color-start: false;
@button__hover__gradient-color-end: false;

@button__active__color: @button__color;
@button__active__background: @button__hover__background;
@button__active__border: @button__border;
@button__active__gradient-color-start: false;
@button__active__gradient-color-end: false;

//  Primary button
@button-primary__line-height: false;
@button-primary__width: false;
@button-primary__margin: false;
@button-primary__padding: @button__padding;
@button-primary__gradient: false;
@button-primary__gradient-direction: false;

@button-primary__background: @color-blue1;
@button-primary__border: 1px solid @color-blue1;
@button-primary__color: @color-white;
@button-primary__gradient-color-start: false;
@button-primary__gradient-color-end: false;

@button-primary__hover__background: @color-blue2;
@button-primary__hover__border: 1px solid @color-blue2;
@button-primary__hover__color: @button-primary__color;
@button-primary__hover__gradient-color-start: false;
@button-primary__hover__gradient-color-end: false;

@button-primary__active__background: @button-primary__hover__background;
@button-primary__active__border: @button-primary__hover__border;
@button-primary__active__color: @button-primary__color;
@button-primary__active__gradient-color-start: false;
@button-primary__active__gradient-color-end: false;

//  Gradient button
@button__gradient: false; // [true|false] - button has a gradient background
@button__gradient-direction: false; // button gradient direction if button has a gradient background

//  Button with icon
@button-icon__use: false;
@button-icon__content: @icon-settings;
@button-icon__font: @icon-font;
@button-icon__font-size: 22px;
@button-icon__line-height: @button-icon__font-size;
@button-icon__color: inherit;
@button-icon__margin: 0;
@button-icon__vertical-align: top;
@button-icon__position: @icon__position;
@button-icon__text-hide: false;

@button-icon__hover__font-color: inherit;
@button-icon__active__font-color: inherit;

//  Large button
@button__font-size__l: @font-size__l;
@button__line-height__l: @font-size__l + 4;
@button__padding__l: 14px 17px;

//  Small button
@button__font-size__s: 11px;
@button__line-height__s: @button__font-size__s + 1;
@button__padding__s: @indent__xs 8px;
````


## Formulaire

`/vendor/magento/magento2-base/lib/web/css/source/lib/variables/_forms.less`

````less
//
//  Form variables
//  _____________________________________________

//
//  Form elements inputs default variables
//  ---------------------------------------------

@form-element-input-type: ''; // [input-text|select|textarea|input-radio|input-checkbox]
@form-element-input__background: @color-white;
@form-element-input__border-color: @color-gray76;
@form-element-input__border: 1px solid @form-element-input__border-color;
@form-element-input__border-radius: 1px;
@form-element-input__height: 32px;
@form-element-input__width: 100%;
@form-element-input__margin: false;
@form-element-input__padding: 0 9px;
@form-element-input__vertical-align: baseline;
@form-element-input__background-clip: padding-box; // [border-box|content-box|padding-box]
@form-element-input__font-size: @font-size__base;
@form-element-input__color: false;
@form-element-input__font-family: @font-family__base;
@form-element-input__font-weight: false;
@form-element-input__font-style: false;
@form-element-input__line-height: @line-height__base;

//  Placeholder
@form-element-input-placeholder__color: @color-gray76;
@form-element-input-placeholder__font-style: @form-element-input__font-style;

//  Disabled state
@form-element-input__disabled__background: @form-element-input__background;
@form-element-input__disabled__border: @form-element-input__border;
@form-element-input__disabled__opacity: .5;
@form-element-input__disabled__color: @form-element-input__color;
@form-element-input__disabled__font-style: @form-element-input__font-style;

//  Focus state
@form-element-input__focus__background: @form-element-input__background;
@form-element-input__focus__border: @form-element-input__border;
@form-element-input__focus__color: @form-element-input__color;
@form-element-input__focus__font-style: @form-element-input__font-style;

//  Form elements choice default variables
@form-element-choice__type: ''; // [radio|checkbox]
@form-element-choice__vertical-align: false;
@form-element-choice__margin: 2px @indent__xs 0 0;
@form-element-choice__disabled__opacity: @form-element-input__disabled__opacity;

//
//  Input-text
//  ---------------------------------------------

@input-text__background: @form-element-input__background;
@input-text__border: @form-element-input__border;
@input-text__border-radius: @form-element-input__border-radius;
@input-text__height: @form-element-input__height;
@input-text__width: @form-element-input__width;
@input-text__margin: @form-element-input__margin;
@input-text__padding: @form-element-input__padding;
@input-text__vertical-align: @form-element-input__vertical-align;
@input-text__background-clip: @form-element-input__background-clip; // [border-box|content-box|padding-box]
@input-text__font-size: @form-element-input__font-size;
@input-text__color: @form-element-input__color;
@input-text__font-family: @form-element-input__font-family;
@input-text__font-weight: @form-element-input__font-weight;
@input-text__font-style: @form-element-input__font-style;
@input-text__line-height: @form-element-input__line-height;

//  Placeholder
@input-text-placeholder__color: @form-element-input-placeholder__color;
@input-text-placeholder__font-style: @form-element-input-placeholder__font-style;

//  Disabled state
@input-text__disabled__background: @form-element-input__disabled__background;
@input-text__disabled__border: @form-element-input__disabled__border;
@input-text__disabled__opacity: @form-element-input__disabled__opacity;
@input-text__disabled__color: @form-element-input__disabled__color;
@input-text__disabled__font-style: @form-element-input__disabled__font-style;

//  Focus state
@input-text__focus__background: @form-element-input__focus__background;
@input-text__focus__border: @form-element-input__focus__border;
@input-text__focus__color: @form-element-input__focus__color;
@input-text__focus__font-style: @form-element-input__focus__font-style;

//
//  Select
//  ---------------------------------------------

@select__background: @form-element-input__background;
@select__border: @form-element-input__border;
@select__border-radius: @form-element-input__border-radius;
@select__height: @form-element-input__height;
@select__width: @form-element-input__width;
@select__margin: @form-element-input__margin;
@select__padding: @indent__xs @indent__s 4px;
@select__vertical-align: @form-element-input__vertical-align;
@select__background-clip: @form-element-input__background-clip; // [border-box|content-box|padding-box]
@select__font-size: @form-element-input__font-size;
@select__color: @form-element-input__color;
@select__font-family: @form-element-input__font-family;
@select__font-weight: @form-element-input__font-weight;
@select__font-style: @form-element-input__font-style;
@select__line-height: @form-element-input__line-height;

//  Placeholder
@select-placeholder__color: false;
@select-placeholder__font-style: false;

//  Disabled state
@select__disabled__background: @form-element-input__disabled__background;
@select__disabled__border: @form-element-input__disabled__border;
@select__disabled__opacity: @form-element-input__disabled__opacity;
@select__disabled__color: @form-element-input__disabled__color;
@select__disabled__font-style: @form-element-input__disabled__font-style;

//  Focus state
@select__focus__background: @form-element-input__focus__background;
@select__focus__border: @form-element-input__focus__border;
@select__focus__color: @form-element-input__focus__color;
@select__focus__font-style: @form-element-input__focus__font-style;

//
//  Textarea
//  ---------------------------------------------

@textarea__background: @form-element-input__background;
@textarea__border: @form-element-input__border;
@textarea__border-radius: @form-element-input__border-radius;
@textarea__height: auto;
@textarea__width: @form-element-input__width;
@textarea__padding: @indent__s;
@textarea__margin: 0;
@textarea__vertical-align: @form-element-input__vertical-align;
@textarea__background-clip: @form-element-input__background-clip; // [border-box|content-box|padding-box]
@textarea__font-size: @form-element-input__font-size;
@textarea__color: @form-element-input__color;
@textarea__font-family: @form-element-input__font-family;
@textarea__font-weight: @form-element-input__font-weight;
@textarea__font-style: @form-element-input__font-style;
@textarea__line-height: @form-element-input__line-height;
@textarea__resize: vertical; // [none|both|horizontal|vertical|inherit]

//  Placeholder
@textarea-placeholder__color: @form-element-input-placeholder__color;
@textarea-placeholder__font-style: @form-element-input-placeholder__font-style;

//  Disabled state
@textarea__disabled__background: @form-element-input__disabled__background;
@textarea__disabled__border: @form-element-input__disabled__border;
@textarea__disabled__opacity: @form-element-input__disabled__opacity;
@textarea__disabled__color: @form-element-input__disabled__color;
@textarea__disabled__font-style: @form-element-input__disabled__font-style;

//  Focus state
@textarea__focus__background: @form-element-input__focus__background;
@textarea__focus__border: @form-element-input__focus__border;
@textarea__focus__color: @form-element-input__focus__color;
@textarea__focus__font-style: @form-element-input__focus__font-style;

//
//  Radio
//  ---------------------------------------------

@input-radio__vertical-align: @form-element-choice__vertical-align;
@input-radio__margin: @form-element-choice__margin;

@input-radio__disabled__opacity: @form-element-choice__disabled__opacity;

//
//  Checkbox
//  ---------------------------------------------

@input-checkbox__vertical-align: @form-element-choice__vertical-align;
@input-checkbox__margin: @form-element-choice__margin;

@input-checkbox__disabled__opacity: @form-element-choice__disabled__opacity;

//
//  Validation
//  ---------------------------------------------

@form-validation-note__color-error: @error__color;
@form-validation-note__font-size: @font-size__s;
@form-validation-note__font-family: false;
@form-validation-note__font-style: false;
@form-validation-note__font-weight: false;
@form-validation-note__line-height: false;
@form-validation-note__margin: 3px 0 0;
@form-validation-note__padding: false;

@form-validation-note-icon__use: false;
@form-validation-note-icon__font-content: @icon-pointer-up;
@form-validation-note-icon__font: @icon-font;
@form-validation-note-icon__font-size: @form-validation-note__font-size * 2;
@form-validation-note-icon__font-line-height: @form-validation-note__font-size;
@form-validation-note-icon__font-color: @form-validation-note__color-error;
@form-validation-note-icon__font-color-hover: false;
@form-validation-note-icon__font-color-active: false;
@form-validation-note-icon__font-margin: false;
@form-validation-note-icon__font-vertical-align: @icon-font__vertical-align;
@form-validation-note-icon__font-position: @icon-font__position;
@form-validation-note-icon__font-text-hide: @icon-font__text-hide;

@form-element-validation__color-error: false;
@form-element-validation__color-valid: false;
@form-element-validation__border-error: lighten(@form-validation-note__color-error, 20%);
@form-element-validation__border-valid: false;
@form-element-validation__background-error: false;
@form-element-validation__background-valid: false;

//
//  Fieldset
//  ---------------------------------------------

@form-fieldset__border: 0;
@form-fieldset__margin: 0 0 @indent__xl;
@form-fieldset__padding: 0;
@form-fieldset-legend__color: false;
@form-fieldset-legend__font-size: 20px;
@form-fieldset-legend__font-family: false;
@form-fieldset-legend__font-weight: false;
@form-fieldset-legend__font-style: false;
@form-fieldset-legend__line-height: 1.2;
@form-fieldset-legend__margin: 0 0 @indent__m;
@form-fieldset-legend__padding: 0;
@form-fieldset-legend__width: false;

//
//  Field
//  ---------------------------------------------

@form-field-type: block; // [inline|block]
@form-field-type-revert: inline; // [inline|block|false]
@form-field__border: false;
@form-field__vertical-indent: @indent__base;
@form-field__additional-vertical-indent: @form-field__vertical-indent/2;
@form-field-type-block__margin: 0 0 @form-field__vertical-indent;
@form-field-type-inline__margin: 0 0 @form-field__vertical-indent;

@form-field-column: false;
@form-field-column__padding: 0 12px 0 0;
@form-field-column__number: 2;

//  Form field label
@form-field-label__align: false;
@form-field-label__color: false;
@form-field-label__font-size: false;
@form-field-label__font-family: false;
@form-field-label__font-weight: @font-weight__bold;
@form-field-label__font-style: false;
@form-field-label__line-height: false;

@form-field-type-label-inline__margin: false;
@form-field-type-label-inline__padding-top: 6px;
@form-field-type-label-inline__padding: @form-field-type-label-inline__padding-top 15px 0 0;
@form-field-type-label-inline__width: 25.8%;
@form-field-type-label-inline__align: right;

@form-field-type-label-block__margin: 0 0 @indent__xs;
@form-field-type-label-block__padding: false;
@form-field-type-label-block__align: @form-field-label__align;

//  Form field control
@form-field-type-control-inline__width: 74.2%;

//  Form field label asterisk
@form-field-label-asterisk__color: @color-red10;
@form-field-label-asterisk__font-size: @font-size__s;
@form-field-label-asterisk__font-family: false;
@form-field-label-asterisk__font-weight: false;
@form-field-label-asterisk__font-style: false;
@form-field-label-asterisk__line-height: false;
@form-field-label-asterisk__margin: 0 0 0 @indent__xs;

//  Form field note
@form-field-note__color: false;
@form-field-note__font-size: @font-size__s;
@form-field-note__font-family: false;
@form-field-note__font-weight: false;
@form-field-note__font-style: false;
@form-field-note__line-height: false;
@form-field-note__margin: 3px 0 0;
@form-field-note__padding: 0;

//  Form field note icon
@form-field-note-icon-font: @icon-font;
@form-field-note-icon-font__content: @icon-pointer-up;
@form-field-note-icon-font__size: @form-field-note__font-size * 2;
@form-field-note-icon-font__line-height: @form-field-note__font-size;
@form-field-note-icon-font__color: @form-field-note__color;
@form-field-note-icon-font__color-hover: false;
@form-field-note-icon-font__color-active: false;
@form-field-note-icon-font__margin: false;
@form-field-note-icon-font__vertical-align: @icon-font__vertical-align;
@form-field-note-icon-font__position: @icon-font__position;
@form-field-note-icon-font__text-hide: @icon-font__text-hide;

//  Hasrequired
@form-hasrequired__position: top; // [top|bottom]
@form-hasrequired__color: @form-field-label-asterisk__color;
@form-hasrequired__font-size: @font-size__s;
@form-hasrequired__font-family: false;
@form-hasrequired__font-weight: false;
@form-hasrequired__font-style: false;
@form-hasrequired__line-height: false;
@form-hasrequired__border: false;
@form-hasrequired__margin: @indent__s 0 0;
@form-hasrequired__padding: false;
````


##Typo

`/vendor/magento/magento2-base/lib/web/css/source/lib/variables/_typography.less`

````less

//
//  Fonts
//  ---------------------------------------------

//  Path
@font-path: "../../fonts/";
@icons__font-path: "@{baseDir}fonts/Blank-Theme-Icons/Blank-Theme-Icons";

//  Names
@icons__font-name: 'icons-blank-theme'; // ToDo UI: we need to rename (it shouldn't use blank theme name) or move icon fonts to blank theme

//  Font families
@font-family__sans-serif: 'Helvetica Neue', Helvetica, Arial, sans-serif;
@font-family__serif: Georgia, 'Times New Roman', Times, serif;
@font-family__monospace: Menlo, Monaco, Consolas, 'Courier New', monospace;

@font-family__base: @font-family__sans-serif;

//  Sizes
@root__font-size: 62.5%; // Defines ratio between root font size and base font size, 1rem = 10px
@font-size-ratio__base: 1.4; // Defines ratio of the root font-size to the base font-size

@font-size-unit: rem; // The unit to which most typography values will be converted by default
@font-size-unit-ratio: unit(@root__font-size * 16/100); // Ratio of the root font-size to the font-size unit
@font-size-unit-convert: true; // Controls whether font-size values are converted to the specified font-size unit

@font-size__base: unit(@font-size-unit-ratio * @font-size-ratio__base, px); // Base font size value in <b>px</b>
@font-size__xl: ceil(1.5 * @font-size__base); // 21px
@font-size__l: ceil(1.25 * @font-size__base); // 18px
@font-size__s: ceil(.85 * @font-size__base); // 12px
@font-size__xs: floor(.75 * @font-size__base); // 11px

//  Weights
@font-weight__light: 300;
@font-weight__regular: 400;
@font-weight__heavier: 500;
@font-weight__semibold: 600;
@font-weight__bold: 700;

//  Styles
@font-style__base: normal;
@font-style__emphasis: italic;

//  Line heights
@line-height__base: 1.428571429;
@line-height__computed: floor(@font-size__base * @line-height__base);
@line-height__xl: 1.7;
@line-height__l: 1.5;
@line-height__s: 1.33;

//  Colors
@text__color: @primary__color;
@text__color__intense: @primary__color__darker;
@text__color__muted: @primary__color__lighter;

//
//  Indents
//  ---------------------------------------------

@indent__base: @line-height__computed; // 20px
@indent__xl: @indent__base * 2; // 40px
@indent__l: @indent__base * 1.5; // 30px
@indent__m: @indent__base * 1.25; // 25px
@indent__s: @indent__base / 2; // 10px
@indent__xs: @indent__base / 4; // 5px

//
//  Borders
//  ---------------------------------------------

@border-color__base: darken(@page__background-color, 18%);
@border-width__base: 1px;

//
//  Links
//  ---------------------------------------------

@link__color: @color-blue1;
@link__text-decoration: none;

@link__visited__color: @link__color;
@link__visited__text-decoration: none;

@link__hover__color: @color-blue2;
@link__hover__text-decoration: underline;

@link__active__color: @active__color;
@link__active__text-decoration: underline;

//
//  Focus
//  ---------------------------------------------

@focus__color: @color-sky-blue1;
@focus__box-shadow: 0 0 3px 1px @focus__color;

//
//  Lists
//  ---------------------------------------------

@list__color__base: false;
@list__font-size__base: false;
@list__margin-top: 0;
@list__margin-bottom: @indent__m;

@list-item__margin-top: 0;
@list-item__margin-bottom: @indent__s;

@dl__margin-top: 0;
@dl__margin-bottom: @indent__base;

@dt__margin-top: 0;
@dt__margin-bottom: @indent__xs;
@dt__font-weight: @font-weight__bold;

@dd__margin-top: 0;
@dd__margin-bottom: @indent__s;

//
//  Paragraphs
//  ---------------------------------------------

@p__margin-top: 0;
@p__margin-bottom: @indent__s;

//
//  Headings
//  ---------------------------------------------

@heading__font-family__base: false;
@heading__font-weight__base: @font-weight__light;
@heading__line-height__base: 1.1;
@heading__color__base: false;
@heading__font-style__base: false;
@heading__margin-top__base: @indent__base;
@heading__margin-bottom__base: @indent__base;

@h1__font-size: ceil((@font-size__base * 1.85)); // 26px
@h1__font-color: @heading__color__base;
@h1__font-family: @heading__font-family__base;
@h1__font-weight: @heading__font-weight__base;
@h1__font-style: @heading__font-style__base;
@h1__line-height: @heading__line-height__base;
@h1__margin-top: 0;
@h1__margin-bottom: @heading__margin-bottom__base;
@h1__font-size-desktop: ceil((@font-size__base * 2.85)); // 40px

@h2__font-size: ceil((@font-size__base * 1.85)); // 26px
@h2__font-color: @heading__color__base;
@h2__font-family: @heading__font-family__base;
@h2__font-weight: @heading__font-weight__base;
@h2__font-style: @heading__font-style__base;
@h2__line-height: @heading__line-height__base;
@h2__margin-top: @indent__m;
@h2__margin-bottom: @heading__margin-bottom__base;

@h3__font-size: ceil((@font-size__base * 1.28)); // 18px
@h3__font-color: @heading__color__base;
@h3__font-family: @heading__font-family__base;
@h3__font-weight: @heading__font-weight__base;
@h3__font-style: @heading__font-style__base;
@h3__line-height: @heading__line-height__base;
@h3__margin-top: @indent__base * .75;
@h3__margin-bottom: @indent__s;

@h4__font-size: @font-size__base; // 14px
@h4__font-color: @heading__color__base;
@h4__font-family: @heading__font-family__base;
@h4__font-weight: @font-weight__bold;
@h4__font-style: @heading__font-style__base;
@h4__line-height: @heading__line-height__base;
@h4__margin-top: @heading__margin-top__base;
@h4__margin-bottom: @heading__margin-bottom__base;

@h5__font-size: ceil((@font-size__base * .85)); // 12px
@h5__font-color: @heading__color__base;
@h5__font-family: @heading__font-family__base;
@h5__font-weight: @font-weight__bold;
@h5__font-style: @heading__font-style__base;
@h5__line-height: @heading__line-height__base;
@h5__margin-top: @heading__margin-top__base;
@h5__margin-bottom: @heading__margin-bottom__base;

@h6__font-size: ceil((@font-size__base * .7)); // 10px
@h6__font-color: @heading__color__base;
@h6__font-family: @heading__font-family__base;
@h6__font-weight: @font-weight__bold;
@h6__font-style: @heading__font-style__base;
@h6__line-height: @heading__line-height__base;
@h6__margin-top: @heading__margin-top__base;
@h6__margin-bottom: @heading__margin-bottom__base;

@heading__small-color: @primary__color;
@heading__small-line-height: 1;
@heading__small-size: (@font-size__xs/@font-size__base) * 100%;

//  Code blocks
@code__background-color: @panel__background-color;
@code__color: @primary__color__darker;
@code__font-size: @font-size__s;
@code__padding: 2px 4px;

@pre__background-color: @panel__background-color;
@pre__border-color: @border-color__base;
@pre__border-width: @border-width__base;
@pre__color: @primary__color__darker;

@kbd__background-color: @panel__background-color;
@kbd__color: @primary__color__darker;

//  Blockquote
@blockquote__border-color: @border-color__base;
@blockquote__border-width: 0;
@blockquote__content-before: '\2014 \00A0';
@blockquote__font-size: @font-size__base;
@blockquote__font-style: @font-style__emphasis;
@blockquote__margin: 0 0 @indent__base @indent__xl;
@blockquote__padding: 0;

@blockquote-small__color: @primary__color;
@blockquote-small__font-size: @font-size__xs;

@cite__font-style: @font-style__base;

//  Misc
@hr__border-color: @border-color__base;
@hr__border-style: solid;
@hr__border-width: @border-width__base;

@mark__color: @primary__color__dark;
@mark__background-color: @panel__background-color;

@abbr__border-color: @border-color__base;

//  Disable filters output in css
@disable-filters: false;

````


## Messages

`/vendor/magento/magento2-base/lib/web/css/source/lib/variables/_messages.less`

````less

//
//  Messages variables
//  _____________________________________________

@message__padding: @indent__s @indent__base;
@message__margin: 0 0 @indent__s;

@message__color: false; // Each message type has its own message color
@message__font-size: 13px;
@message__font-family: false;
@message__font-style: false;
@message__font-weight: false;
@message__line-height: 1.2em;

@message-icon__font-size: ceil(@message__font-size * 2 + 2);
@message-icon__font-line-height: @message-icon__font-size;
@message-icon__inner-padding-left: 40px;
@message-icon__lateral-width: 30px;
@message-icon__lateral-arrow-size: 5px;
@message-icon__top: 18px;
@message-icon__right: false;
@message-icon__bottom: false;
@message-icon__left: 0;

@message__border-width: false;
@message__border-color: false;
@message__border-style: false;
@message__border-radius: false;

//  Information message
@message-info__color: @color-brownie1;
@message-info__background: @color-yellow-light1;
@message-info__border-color: @message__border-color;

@message-info-link__color: @link__color;
@message-info-link__color-hover: @link__hover__color;
@message-info-link__color-active: @link__hover__color;

@message-info-icon: @icon-warning;
@message-info-icon__color-inner: @color-brownie-light1;
@message-info-icon__color-lateral: @color-white;
@message-info-icon__background: @color-brownie1;
@message-info-icon__top: @message-icon__top;
@message-info-icon__right: @message-icon__right;
@message-info-icon__bottom: @message-icon__bottom;
@message-info-icon__left: @message-icon__left;

//  Warning message
@message-warning__color: @message-info__color;
@message-warning__background: @message-info__background;
@message-warning__border-color: @message-info__border-color;

@message-warning-link__color: @message-info-link__color;
@message-warning-link__color-hover: @message-info-link__color-hover;
@message-warning-link__color-active: @message-info-link__color-active;

@message-warning-icon: @message-info-icon;
@message-warning-icon__color-inner: @message-info-icon__color-inner;
@message-warning-icon__color-lateral: @message-info-icon__color-lateral;
@message-warning-icon__background: @message-info-icon__background;
@message-warning-icon__top: @message-icon__top;
@message-warning-icon__right: @message-icon__right;
@message-warning-icon__bottom: @message-icon__bottom;
@message-warning-icon__left: @message-icon__left;

//  Error message
@message-error__color: @error__color;
@message-error__background: @color-pink1;
@message-error__border-color: @message__border-color;

@message-error-link__color: @link__color;
@message-error-link__color-hover: @link__hover__color;
@message-error-link__color-active: @link__hover__color;

@message-error-icon: @icon-warning;
@message-error-icon__color-inner: @color-red11;
@message-error-icon__color-lateral: @color-white;
@message-error-icon__background: @color-red11;
@message-error-icon__top: @message-icon__top;
@message-error-icon__right: @message-icon__right;
@message-error-icon__bottom: @message-icon__bottom;
@message-error-icon__left: @message-icon__left;

//  Success message
@message-success__color: @color-dark-green1;
@message-success__background: @color-gray-light1;
@message-success__border-color: @message__border-color;

@message-success-link__color: @link__color;
@message-success-link__color-hover: @link__hover__color;
@message-success-link__color-active: @link__hover__color;

@message-success-icon: @icon-checkmark;
@message-success-icon__color-inner: @color-dark-green1;
@message-success-icon__color-lateral: @color-white;
@message-success-icon__background: @color-dark-green1;
@message-success-icon__top: @message-icon__top;
@message-success-icon__right: @message-icon__right;
@message-success-icon__bottom: @message-icon__bottom;
@message-success-icon__left: @message-icon__left;

//  Notice message
@message-notice__color: @message-info__color;
@message-notice__background: @message-info__background;
@message-notice__border-color: @message-info__border-color;

@message-notice-link__color: @message-info-link__color;
@message-notice-link__color-hover: @message-info-link__color-hover;
@message-notice-link__color-active: @message-info-link__color-active;

@message-notice-icon: @message-info-icon;
@message-notice-icon__color-inner: @message-info-icon__color-inner;
@message-notice-icon__color-lateral: @message-info-icon__color-lateral;
@message-notice-icon__background: @message-info-icon__background;
@message-notice-icon__top: @message-icon__top;
@message-notice-icon__right: @message-icon__right;
@message-notice-icon__bottom: @message-icon__bottom;
@message-notice-icon__left: @message-icon__left;
````

## Pagination

`/vendor/magento/magento2-base/lib/web/css/source/lib/variables/_pages.less`

````less

//
//  Pager variables
//  _____________________________________________

@pager-label__display: none;
@pager-reset-spaces: true;  // Reset spaces between inline-block elements

@pager__font-size: @font-size__s;
@pager__font-weight: @font-weight__bold;
@pager__line-height: 32px;

@pager-item__display: inline-block;
@pager-item__margin: 0 2px 0 0;
@pager-item__padding: 0 4px;

@pager-actions__padding: 0;

//  Pager current page
@pager-current__font-weight: @font-weight__bold;
@pager-current__color: @primary__color;
@pager-current__border: false;
@pager-current__background: false;
@pager-current__gradient: false;
@pager-current__gradient-direction: false;
@pager-current__gradient-color-start: false;
@pager-current__gradient-color-end: false;

//  Pager link page
@pager__gradient: false;
@pager__gradient-direction: false;

//  Pager link default
@pager__color: @link__color;
@pager__border: false;
@pager__text-decoration: none;
@pager__background: false;
@pager__gradient-color-start: false;
@pager__gradient-color-end: false;

//  Pager link visited
@pager__visited__color: @link__visited__color;
@pager__visited__border: false;
@pager__visited__background: false;
@pager__visited__gradient-color-start: false;
@pager__visited__gradient-color-end: false;

//  Pager link hover
@pager__hover__color: @link__hover__color;
@pager__hover__border: false;
@pager__hover__text-decoration: none;
@pager__hover__background: false;
@pager__hover__gradient-color-start: false;
@pager__hover__gradient-color-end: false;

//  Pager link active
@pager__active__color: @link__active__color;
@pager__active__border: false;
@pager__active__background: false;
@pager__active__gradient-color-start: false;
@pager__active__gradient-color-end: false;

//  Pager link.action
@pager-icon__use: true;
@pager-icon__previous-content: @icon-prev;
@pager-icon__next-content: @icon-next;
@pager-icon__text-hide: true;
@pager-icon__position: before;
@pager-icon__font: @icon-font;
@pager-icon__font-margin: 0 0 0 -6px;
@pager-icon__font-vertical-align: top;
@pager-icon__font-size: 46px;
@pager-icon__font-line-height: @icon-font__line-height;

//  Pager link.action gradient: element has a gradient background
@pager-action__gradient: false; // [true|false]
@pager-action__gradient-direction: false; // [true|false]

//  Pager link.action default
@pager-action__color: @text__color__muted;
@pager-action__border: @border-width__base solid @border-color__base;
@pager-action__text-decoration: @pager__text-decoration;
@pager-action__background: @pager__background;
@pager-action__gradient-color-start: false;
@pager-action__gradient-color-end: false;

//  Pager link.action visited
@pager-action__visited__color: @pager-action__color;
@pager-action__visited__border: false;
@pager-action__visited__background: false;
@pager-action__visited__gradient-color-start: false;
@pager-action__visited__gradient-color-end: false;

//  Pager link.action hover
@pager-action__hover__color: @pager-action__color;
@pager-action__hover__border: false;
@pager-action__hover__background: false;
@pager-action__hover__text-decoration: @pager__hover__text-decoration;
@pager-action__hover__gradient-color-start: false;
@pager-action__hover__gradient-color-end: false;

//  Pager link.action active
@pager-action__active__color: @pager-action__color;
@pager-action__active__border: false;
@pager-action__active__background: false;
@pager-action__active__gradient-color-start: false;
@pager-action__active__gradient-color-end: false;
````

## Et de manière plus générale...

... regarder les fichiers qui sont dans : `/vendor/magento/magento2-base/lib/web/css/source/lib/variables/`