<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>

  Date : 01/10/2018
  Time : 15:02
-->

# Optimisation des caches

Si on bosse que sur du CSS on peut garder une grand partie des caches de Magento. 

Voici un exemple de mon status de cache : 
 
````
Current status:
                        config: 1
                        layout: 1
                    block_html: 1
                   collections: 1
                    reflection: 1
                        db_ddl: 1
                           eav: 1
         customer_notification: 1
            config_integration: 1
        config_integration_api: 1
                     full_page: 0
                     translate: 1
             config_webservice: 1

````

Tous mes caches sont activés sauf `full_page` ainsi l'affichage de la page est beaucoup plus rapide.

Pour rappel : `php bin/magento cache:enable` et la liste des caches à activer.

Si je touche à un layout ou une config il suffit de faire un `php bin/magento cache:flush` 

# Config de Grunt

Dans la config de Grunt on précise la locale. Les CSS sont compilés dans un dossier lié à cette locale. 

Pour connaitre la locale à utiliser il suffit de se connecter au site en front et de regarder le code source : 

````html
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.example.com/pub/static/version1538396992/frontend/Vendor/theme/fr_FR/css/styles-m.css" />
````

Ma locale ici est "fr_FR" donc je dois vérifier que dans mon fichier de configuration ca soit la même : 

Dans : `dev/tools/grunt/configs/themes.js`

````json
module.exports = {
    mon_action: {
        area: 'frontend',
        name: 'Vendor/theme',
        locale: 'fr_FR',
        files: [
            'css/styles-m',
            'css/styles-l'
        ],
        dsl: 'less'
    }
};

````

Une fois que tout est correct de ce côté la compilation avec Grunt est beaucoup plus simple. 

Je fais un petit `grunt refresh` une fois pour le fun. Grunt va tout compiler. 

Ensuite il me suffit de faire `grunt exec:mon_action` dès que j'ajoute des fichiers dans un des dossiers `www`

Dès que je modifie un fichier .less un simple `grunt less:mon_action` suffit.

 