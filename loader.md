<!-- 
 Copyright (c) 2014 - 2018 TBD, SAS. All rights reserved.
 Developer: Pierre-Louis HUBERT <pierre-louis.hubert@agence-tbd.com>
-->
# Loader Magento

Magento propose un widget Loader. 

Par défaut il est déjà initialisé sur le body. On peut donc simplement l'afficher de la façon suivante : 

````javascript
$('body').loader('show');
// ou
$('body').trigger('processStart');
```` 

Et le masquer

````javascript
$('body').loader('hide');
// ou
$('body').trigger('processStop');
```` 

Mais on peut également se créer un loader spécifique à un élément HTML en particulier : 

````html
<div id="myDiv" data-mage-init='{"loader" : {"icon" : "http://url-to-spinner.com/spinner.gif"}}'>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
</div>
````

Et on peut donc faire : 

````javascript
$('#myDiv').loader('show');
$('#myDiv').trigger('processStart');

$('#myDiv').loader('hide');
$('#myDiv').trigger('processStop');
````

Par défaut le loaer de Magento a une position fixed on peut donc utiliser les classes suivantes pour le contraindre dans l'élément : 

````html
<div id="myDiv" data-mage-init='{"loader" : {"icon" : "http://url-to-spinner.com/spinner.gif"}}' class="has_own_loader">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
</div>
````

````less
.has_own_loader {
    position: relative;

    .loading-mask {
        position: absolute;
        .loader {
            & > img {
                position: absolute;
            }
        }
    }
}
````

Voir la doc complète ici : <https://devdocs.magento.com/guides/v2.3/javascript-dev-guide/widgets/widget_loader.html>